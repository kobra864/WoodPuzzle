﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ItemSlot : MonoBehaviour//, IDropHandler
{   
    [SerializeField] private DragController _dragController;
    [SerializeField] private GameObject diamond;

    private void OnMouseOver()
    {
        if (Input.GetMouseButtonUp(0) && _dragController.HasSelectedItem())
        {            
            GameObject item = _dragController.TakeSelectedItem().gameObject;
            Destroy(item);

            diamond.transform.localScale = new Vector3(0.03f, 0.03f, 0.03f);
            Instantiate(diamond, transform);           
        }
    }
}
